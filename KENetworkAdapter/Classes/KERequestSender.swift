//
//  KERequestSender.swift
//  KENetworkAdapter
//
//  Created by Kozlov Evgenii on 27/09/2018.
//

import Foundation

public protocol KERequestSending {
    var sessionConfiguration: URLSessionConfiguration { get set }
    var errorInterpreter: KEErrorInterpreting? { get set }
    func send(request: KERequestType, completion: ((Result<Data?, Error>) -> ())?)
}

public extension KERequestSending {
    
    var errorInterpreter: KEErrorInterpreting? {
        return nil
    }
    
}

public struct KERequestSender: KERequestSending {
    
    public var sessionConfiguration: URLSessionConfiguration
    public var errorInterpreter: KEErrorInterpreting?
    
    public init(with sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default, and errorInterpreter: KEErrorInterpreting? = nil) {
        self.sessionConfiguration = sessionConfiguration
        self.errorInterpreter = errorInterpreter
    }
    
    public func send(request: KERequestType, completion: ((Result<Data?, Error>) -> ())?) {
        let urlRequest = request.request
        URLSession(configuration: sessionConfiguration).dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                if let networkError = self.errorInterpreter?.error(with: response, data: data) {
                    completion?(Result.failure(networkError))
                } else {
                    completion?(Result.failure(error))
                }
            } else {
                if let networkError = self.errorInterpreter?.error(with: response, data: data) {
                    completion?(Result.failure(networkError))
                } else {
                    completion?(Result.success(data))
                }
            }
        }.resume()
    }
    
}
