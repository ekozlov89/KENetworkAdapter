//
//  KERequestDecorator.swift
//  KENetworkAdapter
//
//  Created by Kozlov Evgenii on 27/09/2018.
//

import Foundation

public protocol KERequestDecorator: KERequestType {
    var target: KERequestType { get }
}

public extension KERequestDecorator {
    
    var method: HTTPMethod {
        get { return target.method }
    }
    
    var request: URLRequest {
        get { return target.request }
    }
    
    var url: URL {
        get { return target.url }
    }
    
}
