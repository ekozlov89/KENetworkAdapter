//
//  URL+Extension.swift
//  KENetworkAdapter
//
//  Created by Kozlov Evgenii on 27/09/2018.
//

import Foundation

public extension URL {
    
    var queryItems: [URLQueryItem] {
        return URLComponents(url: self, resolvingAgainstBaseURL: true)?.queryItems ?? []
    }
    
    func appendingQueryItems(_ queryItems: [URLQueryItem]) -> URL {
        guard var components: URLComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else { return self }
        if (components.queryItems != nil) {
            components.queryItems?.append(contentsOf: queryItems)
        } else {
            components.queryItems = queryItems
        }
        return components.url ?? self
    }
    
}
