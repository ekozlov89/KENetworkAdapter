//
//  KESafe.swift
//  KENetworkAdapter
//
//  Created by Козлов Евгений on 17.12.2019.
//

import Foundation

public struct KESafe<Base: Decodable>: Decodable {
    
    let value: Base?

    public init(from decoder: Decoder) throws {
        do {
            let container = try decoder.singleValueContainer()
            self.value = try container.decode(Base.self)
        } catch {
            // TODO: automatically send a report about a corrupted data
            print(error)
            self.value = nil
        }
    }
    
}
