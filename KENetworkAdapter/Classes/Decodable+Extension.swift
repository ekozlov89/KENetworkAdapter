//
//  Decodable+Extension.swift
//  KENetworkAdapter
//
//  Created by Kozlov Evgenii on 27/09/2018.
//

import Foundation

public extension Decodable {
        
    init(data: Data?, keyPath: String? = nil) throws {
        guard let data = data else { throw NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data is nil"]) }
        
        if let keyPath = keyPath {
            let topLevel = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (topLevel as AnyObject).value(forKeyPath: keyPath) else { throw NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : "Not found object for keypath: \(keyPath)"]) }
            let nestedData = try JSONSerialization.data(withJSONObject: nestedJson)
            self = try Self.init(data: nestedData)
        } else {
            self = try JSONDecoder().decode(Self.self, from: data)
        }
    }
    
}

public extension Array where Element: Decodable {
    
    init(data: Data?, keyPath: String? = nil, safe: Bool = false) throws {
        guard let data = data else { throw NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data is nil"]) }
        
        if let keyPath = keyPath {
            let topLevel = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (topLevel as AnyObject).value(forKeyPath: keyPath) else { throw NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey : "Not found object for keypath: \(keyPath)"]) }
            let nestedData = try JSONSerialization.data(withJSONObject: nestedJson)
            self = try [Element](data: nestedData, keyPath: nil, safe: safe)
        } else {
            if safe {
                self = (try JSONDecoder().decode([KESafe<Element>].self, from: data)).compactMap({ $0.value })
            } else {
                self = try JSONDecoder().decode([Element].self, from: data)
            }
        }
        
    }
    
}
