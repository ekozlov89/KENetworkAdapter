//
//  KEErrorInterpreting.swift
//  KENetworkAdapter
//
//  Created by Kozlov Evgenii on 27/09/2018.
//

import Foundation

public protocol KEErrorInterpreting {
    func error(with response: URLResponse?, data: Data?) -> Error?
}
