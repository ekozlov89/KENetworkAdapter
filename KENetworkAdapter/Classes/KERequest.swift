//
//  KERequest.swift
//  KENetworkAdapter
//
//  Created by Kozlov Evgenii on 27/09/2018.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case head = "HEAD"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case trance = "TRANCE"
    case options = "OPTIONS"
    case connect = "CONNECT"
    case patch = "PATCH"
}

public protocol KERequestType {
    var url: URL { get }
    var method: HTTPMethod { get }
    var request: URLRequest { get }
}

public struct KERequest: KERequestType {
    
    public var method: HTTPMethod
    public var url: URL
    
    public init(with method: HTTPMethod, url: URL) {
        self.method = method
        self.url = url
    }
    
    public var request: URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        return request
    }
    
}
