//
//  KERequestSenderDecorator.swift
//  KENetworkAdapter
//
//  Created by Kozlov Evgenii on 27/09/2018.
//

import Foundation

public protocol KERequestSenderDecorator: KERequestSending {
    var target: KERequestSending { get set }
}

public extension KERequestSenderDecorator {
    
    var errorInterpreter: KEErrorInterpreting? {
        get { return target.errorInterpreter }
        set { target.errorInterpreter = newValue }
    }
    
    func send(request: KERequestType, completion: ((Result<Data?, Error>) -> ())?) {
        target.send(request: request, completion: completion)
    }
    
}
