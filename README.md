# KENetworkAdapter

[![CI Status](https://img.shields.io/travis/Kozlov Evghenii/KENetworkAdapter.svg?style=flat)](https://travis-ci.org/Kozlov Evghenii/KENetworkAdapter)
[![Version](https://img.shields.io/cocoapods/v/KENetworkAdapter.svg?style=flat)](https://cocoapods.org/pods/KENetworkAdapter)
[![License](https://img.shields.io/cocoapods/l/KENetworkAdapter.svg?style=flat)](https://cocoapods.org/pods/KENetworkAdapter)
[![Platform](https://img.shields.io/cocoapods/p/KENetworkAdapter.svg?style=flat)](https://cocoapods.org/pods/KENetworkAdapter)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KENetworkAdapter is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KENetworkAdapter'
```

## Author

Kozlov Evghenii, ekozlov89@mail.ru

## License

KENetworkAdapter is available under the MIT license. See the LICENSE file for more info.
